import React from 'react';
import Menu from './Menu';
import Matrix from './Matrix';
import '../App.css';

class Content extends React.Component{
    constructor(props){
        super(props);
        this.saveKey = 'matrix_app_data';
        let savedState = window.sessionStorage.getItem(this.saveKey);
        let state;
        if(savedState == null){
            state = {
                n: 1,
                items: [
                    [
                        {
                            id: '0-0',
                            i: 0,
                            j: 0,
                            val: '',
                            isColor: false
                        }
                    ]
                ]
            };
        }else{
            state = JSON.parse(savedState);
        }
        this.state = state;
        this.drawMatrix = this.drawMatrix.bind(this);
        this.setItemsValues = this.setItemsValues.bind(this);
        this.setItemsColors = this.setItemsColors.bind(this);
    }

    render(){
        this.saveState();
        let data = this.state;
        return (
            <div className="content">
                <Menu num={data.n}
                      onNumberChanged={this.drawMatrix}
                      onButtonFillClicked={this.setItemsValues}
                      onButtonColorsClicked={this.setItemsColors}
                />
                <div id="matrix" className="matrix-container centered">
                    <Matrix data={data} />
                </div>
            </div>
        );
    }

    drawMatrix(e){
        let val = parseInt(e.target.value);
        let items = [];
        for(let i = 0; i < val; i++){
            let rowItem = [];
            for(let j = 0; j < val; j++){
                rowItem.push({
                    id: i + '-' + j,
                    i: i,
                    j: j,
                    val: '',
                    isColor: false
                })
            }
            items.push(rowItem);
        }
        this.setState({
            n: val,
            items: items
        });
    }

    setItemsValues(){
        let items = this.state.items;
        let miltVal = Math.pow(10, this.state.n);
        for(let i = 0; i < this.state.n; i++){
            let tmp = String(Math.floor(Math.random() * miltVal)).padStart(this.state.n, '0');
            for(let j = 0; j < this.state.n; j++){
                items[i][j].val = parseInt(tmp[j]) % 2;
                items[i][j].isColor = false;
            }
        }
        this.setState({
            items: items
        });
    }

    setItemsColors(){
        let items = this.state.items;
        for(let i = 0; i < this.state.n; i++){
            for(let j = 0; j < this.state.n; j++){
                if(items[i][j].val === 1){
                    switch(true){
                        case i > 0 && items[i - 1][j].val === 1:
                            items[i][j].isColor = true;
                            break;
                        case i < this.state.n - 1 && items[i + 1][j].val === 1:
                            items[i][j].isColor = true;
                            break;
                        case j > 0 && items[i][j - 1].val === 1:
                            items[i][j].isColor = true;
                            break;
                        case j < this.state.n - 1 && items[i][j + 1].val === 1:
                            items[i][j].isColor = true;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        this.setState({
            items: items
        });
    }

    saveState(){
        window.sessionStorage.setItem(this.saveKey, JSON.stringify(this.state))
    }
}

export default Content
