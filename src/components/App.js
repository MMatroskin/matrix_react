import React from 'react';
import Content from './Content';
import '../App.css';


function App() {
    let content =
        <div className="App">
            <header>
                <h1>Matrix :)</h1>
            </header>
            <Content />
        </div>;
    return (content);
}

export default App;
