import React from 'react';
import '../App.css';

class Item extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        let data = this.props.data;
        let classList = ['matrix-item'];
        if(data.isColor){
            classList.push('colored')
        }
        const item =
            <div
                data-w={data.j}
                data-h={data.i}
                className={classList.join(' ')}>
                {data.val}
            </div>;
        return (item);
    };
}


export default Item;
