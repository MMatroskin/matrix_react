import React from 'react';
import Row from './Row';
import '../App.css';


function Matrix(props){
    const matrix = props.data;
    let width = String(matrix.n * 48) + 'px';
    let rows = [];
    for (let i = 0; i < matrix.n; i++){
        let dataRow = {i: i, n: matrix.n, items: matrix.items[i]};
        let row =
            <div data-i={dataRow.i} key={'row-' + i}>
                <Row data={dataRow}/>
            </div>;
        rows.push(row)
    }

    return(
        <div className="centered matrix-box" style={{width: width}}>
            {rows}
        </div>
    )
}

export default Matrix;


