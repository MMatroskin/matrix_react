import React from 'react';
import Item from './Item';
import '../App.css';


function Row(props){
    const row = props.data;
    let items = [];
    for(let j = 0; j < row.n; j++){
        let dataItem = row.items[j];
        let item = <Item data={dataItem} key={dataItem.id}/>;
        items.push(item)
    }
    return (
        <div className="matrix-row">
            {items}
        </div>
    );
}

export default Row;
