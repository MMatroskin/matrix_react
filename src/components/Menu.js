import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import '../App.css';


class Menu extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        console.log(this.props);
        const {num, onNumberChanged, onButtonFillClicked, onButtonColorsClicked} = this.props;
        const menuItem =
            <div className="row-items centered">
                <div>
                    <p>Размерность матрицы</p>
                </div>
                <TextField
                    className="w-128"
                    min="1"
                    max="16"
                    value={num}
                    id="numbers"
                    label="1 - 16"
                    type="number"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                    onChange={onNumberChanged}
                />
                <Button variant="contained" className="w-128 h-42" color="primary" id="fill-button" onClick={onButtonFillClicked}>Заполнить</Button>
                <Button variant="contained" className="w-128 h-42" color="primary" id="color-button" onClick={onButtonColorsClicked}>Закрасить</Button>
            </div>;
        return (menuItem);
    }
}

export default Menu;
